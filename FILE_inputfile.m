%% INPUT FILE

%% Simulation parameter

n_rad = 600
radpos = 1;
params.radpos = radpos;
params.n_rad = n_rad;
th_start = 92*pi/180;
th_end =  270*pi/180;
params.th_start = th_start;
params.th_end = th_end;