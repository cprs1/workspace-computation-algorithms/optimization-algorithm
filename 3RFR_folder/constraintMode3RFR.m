function [c,ceq,gradc,gradceq] = constraintMode3RFR(y,geometry,L,Kee,thd,wp,wd,Nf)
c = [];
gradc = [];

qpd = [y(1+3+3*Nf:3+3*Nf+2,1);thd];

[eq,gradeq] = InverseMode3RFR(y,geometry,L,Kee,qpd,wp,wd,Nf);

ceq = [eq(1:end-3);eq(end)]; % fixed orientation

gradceq = [gradeq(1:end-3,:);gradeq(end,:)]';

end