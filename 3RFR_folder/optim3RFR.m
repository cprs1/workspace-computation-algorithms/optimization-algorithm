function [err,graderr] = optim3RFR(y,v,Nf)

pend = y(1+3+3*Nf:3+3*Nf+2,1);

err = (pend(1)-v(1))^2+(pend(2)-v(2))^2;

graderr = [zeros(3+3*Nf,1);2*(pend(1)-v(1));2*(pend(2)-v(2));0;zeros(2*3,1)];


end