
%% Robot Material Parameters
L = 1;
r = 0.001;
E = 210*10^9;
A = pi*r^2;
I = 0.25*pi*r^4;
rho = 7800;
EI = E*I;
rB = 0.60;   % base radius [m]
rP = 0.15;   % platform radius [m]

th1 = pi/2; % wrt fixed frame 
th2 = pi/2+2*pi/3;
th3 = pi/2-2*pi/3;

A1 = rB*[cos(th1);sin(th1)]; % wrt fixed frame 
A2 = rB*[cos(th2);sin(th2)]; 
A3 = rB*[cos(th3);sin(th3)];

B1 = rP*[cos(th1);sin(th1)]; % wrt platform frame 
B2 = rP*[cos(th2);sin(th2)]; 
B3 = rP*[cos(th3);sin(th3)];

basepoints = [A1,A2,A3];
platpoints = [B3,B1,B2];
geometry.basepoints = basepoints;
geometry.platpoints = platpoints;

stresslim = +Inf; % Pa

%% External loads

g = [0;0];
fd = rho*A*g;
fend = [0;0];
mend = 0;

wp = [mend;fend];
wd = [0;fd]; % distributed wrench

%% Model Parameters

Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

th1lim = [0,2*pi];
th2lim = [0,2*pi];
th3lim = [0,2*pi];

pstart = [0;0];
thp = 0*pi/180;
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,v) optim3RFR(y,v,Nf);
fcn.constraints = @(y) constraintMode3RFR(y,geometry,L,Kad,thp,wp,wd,Nf);
fcn.savefcn = @(y) savefcn(y,Nf);

%% First Initial Guess

qa0 = [0;120;240]*pi/180;
qe0 = zeros(3*Nf,1);
qp0 = [pstart;thp];
lambda0 = zeros(6,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseMode3RFR(y,geometry,L,Kad,[pstart;thp],wp,wd,Nf);
options = optimoptions('fsolve','display','iter','Maxiterations',50,'SpecifyObjectiveGradient',true,'CheckGradients',true);

[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
params.y0 = y;
[pos1,pos2,pos3] = pos3RFRmode(y,L,geometry.basepoints,Nf);

Plot3RFR(pos1,pos2,pos3,pstart,thp,geometry,L);
title('Initial Guess Configuration')
drawnow
axis equal
axis([-1 1 -1 1])

