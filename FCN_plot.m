function areatot = FCN_plot(WK)

%% NUMERICAL RESULT
figure()

plot(WK(:,2),WK(:,3),'b.')

xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis([-1 1 -1 1])
title('Workspace Computation')

figure()
[k,areatot] = boundary(WK(:,2),WK(:,3),.6);
if numel(k)>1
    polyshapes = polyshape(WK(k,2),WK(k,3));
    plot(polyshapes)
end

xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis([-1 1 -1 1])
title('Workspace Boundary')


end