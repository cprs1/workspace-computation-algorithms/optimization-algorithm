function [WK,outstruct] = FCN_OptimWK(fcn,params,instantplot)
tic;


%% COLLECT INPUTS
y0 = params.y0;
y0_start = y0;
radpos = params.radpos;
n_rad = params.n_rad;
th_start = params.th_start;
th_end = params.th_end;
savefcn = fcn.savefcn;

%% INITIALIZE
idx = 1:1:n_rad;
th = linspace(th_start,th_end,n_rad+1);
th = th(1:end-1);

WK = [idx',zeros(n_rad,2)];
options = optimoptions('fmincon','display','off','TolFun',1e-6,'SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true,'CheckGradients',false);
%% RAD POINTS
for i = 1:n_rad
    
radpoint = radpos*[cos(th(i));sin(th(i))];

objfun = fcn.objetivefcn;
fun = @(y) objfun(y,radpoint);

nonlcon = fcn.constraints;

y_opt = fmincon(fun,y0,[],[],[],[],[],[],nonlcon,options);

if norm(y_opt-y0)>10^-12
   y0 = y_opt; 
else
   y0 = y0_start;
end
WK(i,2:3) = savefcn(y_opt);

% facultative: PLOT
if instantplot==1
   plot(WK(i,2),WK(i,3),'r.')
   hold on
   plot(radpoint(1),radpoint(2),'k*')
   drawnow
end

end

time = toc;
outstruct.time = time;

end