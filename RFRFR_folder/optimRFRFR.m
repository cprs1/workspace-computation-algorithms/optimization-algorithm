function [err,graderr] = optimRFRFR(y,v,Nf)

pend = y(1+2+2*Nf:2+2*Nf+2,1);

err = (pend(1)-v(1))^2+(pend(2)-v(2))^2;

graderr = [zeros(2+2*Nf,1);2*(pend(1)-v(1));2*(pend(2)-v(2));zeros(2*2,1)];


end