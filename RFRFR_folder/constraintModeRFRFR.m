function [c,ceq,gradc,gradceq] = constraintModeRFRFR(y,geometry,L,Kad,fend,fd,Nf)
c = [];
gradc = [];

pstart = y(1+2+2*Nf:2+2*Nf+2,1);

[eq,gradeq] =  InverseModeForwBackRFRFSeqn(y,geometry,Kad,L,fend,fd,pstart,Nf);

ceq = eq(1:end-2); % fixed orientation

gradceq = gradeq(1:end-2,:)';
